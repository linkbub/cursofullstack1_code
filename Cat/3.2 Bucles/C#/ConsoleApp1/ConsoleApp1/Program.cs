﻿// C#
using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            //Loop1();
            Loop2();
        }

        private static void Loop1()
        {
            var llista = new List<string>();

            llista.Add("ploma");
            llista.Add("bitcoin");
            llista.Add("pizza");

            for (var i = 0; i < llista.Count; i++)
            {
                var paraula = llista[i];
                Console.WriteLine($"la paraula en la posició {i} és " + paraula);
            }

            var condition = true;
            var j = 0;

            while (condition)
            {
                var paraula = llista[j];

                if (paraula.Contains("i"))
                {
                    condition = false;
                    Console.WriteLine($"hem trobat la lletra prohibida en la paraula {paraula} en la posició {j}");
                }
                else
                {
                    Console.WriteLine($"la paraula en la posició {j} és " + paraula);
                    j++;
                }
            }
        }

        private static void Loop2()
        {
            var llista = new List<string>();

            var keepdoing = true;

            while (keepdoing)
            {
                Console.WriteLine("Introdueixi una paraula i escrigui EXIT per a sortir");
                var paraula = Console.ReadLine();

                if (paraula == "EXIT")
                {
                    keepdoing = false;
                }
                else
                {
                    llista.Add(paraula);
                }
            }

            Console.WriteLine("Imprimim les paraules usant el bucle for");
            for (var i = 0; i < llista.Count; i++)
            {
                Console.WriteLine(llista[i]);
            }

            Console.WriteLine("Imprimim les paraules usant el bucle foreach");
            foreach (var paraula in llista)
            {
                Console.WriteLine(paraula);
            }
        }
    }
}