﻿// C#
using ConsoleApp1.Lib.Models;
using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    /// <summary>
    /// L'objectiu d'aquesta iteració és usar classes per a definir:
    /// Students (Estudiant)
    /// Subjects (Matèries)
    /// Exams (Exàmens)
    /// i afegir opcions extres al menú per a poder manejar-les
    /// En l'exemple crearem les entitats (classes que defineixen el domini -funcionalitat- del nostre programa)
    /// i l'opció de menú per a afegir estudiants
    /// L'alumne ha de completar la resta de funcionalitats CRUD (Create, Read, Update, Destroy)
    /// </summary>
    class Program
    {
        //static List<double> Marks { get; set; }

        public static Dictionary<string, Student> Students = new Dictionary<string, Student>();


        static string EscapeWord = "RUNYOUFOOLS";

        static void Main(string[] args)
        {
            Console.WriteLine("Benvingut al programa per a la gestió d'alumnes");
            Console.WriteLine("Per a anar a la gestió d'alumnes usi l'opció a");
            Console.WriteLine("Per a obtenir estadístiques usi l'opció e");

            //Marks = new List<double>();
            var keepdoing = true;

            while (keepdoing)
            {
                var option = Console.ReadKey().KeyChar;

                if (option == 'a')
                {
                    ShowStudentsMenu();
                }
                else if (option == 'e')
                {
                    ShowStatsMenu();
                }
            }


        }

        static void ShowStudentsMenu()
        {
            Console.WriteLine();
            ShowStudentsMenuOptions();

            var keepdoing = true;
            while (keepdoing)
            {
                var text = Console.ReadLine();

                switch (text)
                {
                    casi "all":
                        ShowAllStudents();
                break;
                casi "add":
                        AddNewStudent();
                break;
                casi "m":
                        keepdoing = false;
                break;
                default:
                        AddMark(text);
                break;
            }
        }

        ShowMainMenu();
    }

    private static void ShowStudentsMenuOptions()
    {
        Console.WriteLine("--Menu d'Estudiants--");

        Console.WriteLine("Per a veure tots els estudiants escrigui all");
        Console.WriteLine("Per a afegir un nou estudiant escrigui add");
        Console.WriteLine("Per a editar un estudiant escrigui edit + el dni");
        Console.WriteLine("Per a esborrar un estudiant escrigui del + el dni");
        Console.WriteLine("Per a tornar al menú principal escrigui m");
    }

    private static void ShowMainMenu()
    {
        Console.WriteLine("Volta al menú principal");
        Console.WriteLine("Per a introduir notes dels alumnes usi l'opció a");
        Console.WriteLine("Per a obtenir estadístiques usi l'opció e");
    }

    static void ShowAllStudents()
    {
        foreach (var student in Students.Values)
        {
            Console.WriteLine($"{student.Dni} {student.name}");
        }
    }

    static void AddNewStudent()
    {
        Console.WriteLine("Primer insereixi el dni o escrigui anul·lar per a interrompre");

        var keepdoing = true;
        while (keepdoing)
        {
            var dni = Console.ReadLine();

            if (dni == "anul·lar")
            {
                break;
            }
            else if (string.IsNullOrEmpty(dni) || dni.Length != 9)
            {
                Console.WriteLine("El dni està en un format incorrecte");
            }
            else if (Students.ContainsKey(dni))
            {
                Console.WriteLine($"Ja existeix un estudiant amb el dni {dni}");
            }
            else
            {
                while (true)
                {
                    Console.WriteLine("Ara insereixi el nom o escrigui anul·lar per a interrompre");
                    var name = Console.ReadLine();

                    if (name == "anul·lar")
                    {
                        keepdoing = false;
                        break;
                    }

                    if (string.IsNullOrEmpty(name))
                    {
                        Console.WriteLine("El nom està buit");
                    }
                    else
                    {
                        var student = new Student
                        {
                            Aneu = Guid.NewGuid(),
                            Dni = dni,
                            Name = name
                        };
                        Students.Add(student.Dni, student);
                        keepdoing = false;
                        break;
                    }
                }
            }
        }

        ShowStudentsMenuOptions();
    }

    static void AddMark(string text)
    {
        var mark = 0.0;

        if (double.TryParse(text, out mark))
        {
            //Marks.Add(mark);
            Console.WriteLine("Nota introduïda correctament, afegeixi una altra nota més, premi all per a llesta totes les notes o premi m per a tornar al menú principal");
        }
        else
        {
            Console.WriteLine($"La nota introduïda {text} no està en un format correcte, torni a introduir-la correctament");
        }
    }

    static void ShowStatsMenu()
    {
        Console.WriteLine();
        Console.WriteLine("--Menu d'Estadístiques--");

        Console.WriteLine("Per a veure la mitjana escrigui avg");
        Console.WriteLine("Per a veure la nota més alta escrigui max");
        Console.WriteLine("Per a veure la nota més baixa escrigui min");
        Console.WriteLine("Per a tornar al menú principal escrigui m");

        var keepdoing = true;
        while (keepdoing)
        {
            var text = Console.ReadLine();

            switch (text)
            {
                    casi "avg":
                        ShowAverage();
            break;
            casi "max":
                        ShowMaximum();
            break;
            casi "min":
                        ShowMinimum();
            break;
            casi "m":
                        keepdoing = false;
            break;
            default:
                        Console.WriteLine("comando no reconegut, introdueixi una opció vàlida");
            break;
        }
    }
    ShowMainMenu();
}

static void ShowAverage()
{
    var suma = 0.0;
    for (var i = 0; i < Marks.Count; i++)
    {
        suma += Marks[i];
    }

    var average = suma / Marks.Count;
    Console.WriteLine("la mitjana els exàmens és: {0}", average);
}

static void ShowMaximum()
{
    var max = 0.0;
    for (var i = 0; i < Marks.Count; i++)
    {
        if (Marks[i] > max)
            max = Marks[i];
    }

    Console.WriteLine("la nota més alta és: {0}", max);
}

static void ShowMinimum()
{
    var min = 0.0;
    if (Marks.Count == 0)
        min = 0.0;
    else
        min = Marks[0];

    això(l'operador ternari) fa el mateix que això de dalt

   var min = Marks.Count == 0 ? 0.0 : Marks[0];

    for (var i = 0; i < Marks.Count; i++)
    {
        if (Marks[i] < min)
            min = Marks[i];
    }

    Console.WriteLine("la nota més baixa és: {0}", min);
}
    }
}