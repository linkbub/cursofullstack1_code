﻿// C#
using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var num1 = 5;
            var num2 = 6;

            var suma = num1 + num2;
            var resta = num1 - num2;
            var multiplicacio = num1 * num2;
            var div = num1 / num2;

            var operacio1 = (5 + 8) / 5 * 7;


            var missatge = "Hola ";
            var nom = "Gandalf";

            var text = missatge + nom;

            //var text2 = missatge - nom; error

            num1 = num1 + 3;
            num1 += 3;
            num1 = 2;

            var num3 = num1++; //num + 1;
        }
    }
}