﻿// C#
using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Benvinguts al programa per a la gestió d'alumnes");
            Console.WriteLine("Introdueixi les notes dels alumnes");


            // 1 canviar la grandària de l'array a 10 i ficar 10 notes en comptes de 5
            var notasAlumnes = new double[5];

            notasAlumnes[0] = 4.6;
            notasAlumnes[1] = 2.3;
            notasAlumnes[2] = 6;
            notasAlumnes[3] = 3.9;
            notasAlumnes[4] = 8.5;

            // 2 ajustar la mitjana
            var suma = notasAlumnes[0] +
                        notasAlumnes[1] +
                        notasAlumnes[2] +
                        notasAlumnes[3] +
                        notasAlumnes[4];

            var mitja = suma / notasAlumnes.Length;
            Console.WriteLine($"la mitjana és {mitja}");

            // 3 extreure la nota més alta del array i ensenyar-la en pantalla

            // 4 extreure la nota més baixa del array i ensenyar-la en pantalla
        }

    }
}