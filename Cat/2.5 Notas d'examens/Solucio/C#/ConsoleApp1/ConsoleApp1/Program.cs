﻿using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Benvinguts al programa per a la gestió d'alumnes");
            Console.WriteLine("Introdueix les notes dels alumnes");


            // 1 canviar la grandària de l'array a 10 i ficar 10 notes en comptes de 5
            var notesAlumnes = new double[10];

            notesAlumnes[0] = 4.6;
            notesAlumnes[1] = 2.3;
            notesAlumnes[2] = 6;
            notesAlumnes[3] = 3.9;
            notesAlumnes[4] = 8.5;
            notesAlumnes[5] = 3.6;
            notesAlumnes[6] = 2.6;
            notesAlumnes[7] = 2;
            notesAlumnes[8] = 3.9;
            notesAlumnes[9] = 5.5;

            // 2 ajustar la mitja
            var suma = notesAlumnes[0] +
                        notesAlumnes[1] +
                        notesAlumnes[2] +
                        notesAlumnes[3] +
                        notesAlumnes[4] +
                        notesAlumnes[5] +
                        notesAlumnes[6] +
                        notesAlumnes[7] +
                        notesAlumnes[8] +
                        notesAlumnes[9];

            var mitja = suma / notesAlumnes.Length;
            Console.WriteLine($"la mitja és {mitja}");

            // 3 extreure la nota més alta de l'array i ensenyar-la en pantalla

            var max = notesAlumnes[0];

            if (notesAlumnes[1] > max)
                max = notesAlumnes[1];

            if (notesAlumnes[2] > max)
                max = notesAlumnes[2];

            if (notesAlumnes[3] > max)
                max = notesAlumnes[3];

            if (notesAlumnes[4] > max)
                max = notesAlumnes[4];

            if (notesAlumnes[5] > max)
                max = notesAlumnes[5];

            if (notesAlumnes[6] > max)
                max = notesAlumnes[6];

            if (notesAlumnes[7] > max)
                max = notesAlumnes[7];

            if (notesAlumnes[8] > max)
                max = notesAlumnes[8];

            if (notesAlumnes[9] > max)
                max = notesAlumnes[9];


            Console.WriteLine($"el valor màxim és {max}");

            // 4 extreure la nota més baixa de l'array i ensenyar-la en pantalla
            var min = notesAlumnes[0];

            if (notesAlumnes[1] < min)
                min = notesAlumnes[1];

            if (notesAlumnes[2] < min)
                min = notesAlumnes[2];

            if (notesAlumnes[3] < min)
                min = notesAlumnes[3];

            if (notesAlumnes[4] < min)
                min = notesAlumnes[4];

            if (notesAlumnes[5] < min)
                min = notesAlumnes[5];

            if (notesAlumnes[6] < min)
                min = notesAlumnes[6];

            if (notesAlumnes[7] < min)
                min = notesAlumnes[7];

            if (notesAlumnes[8] < min)
                min = notesAlumnes[8];

            if (notesAlumnes[9] < min)
                min = notesAlumnes[9];


            Console.WriteLine($"el valor mínim és {min}");
        }
    }
}
