﻿// C#
using System.Windows;

namespace Proposta_Calculadora
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        public void Suma()
        {
            var num1 = double.Parse(Num1.Text);
            var num2 = double.Parse(Num2.Text);

            var suma = num1 + num2;
            MessageBox.Show($"la suma és {suma}");
        }

        private void Button_Clic(object sender, RoutedEventArgs e)
        {
            Suma();
        }
    }
}