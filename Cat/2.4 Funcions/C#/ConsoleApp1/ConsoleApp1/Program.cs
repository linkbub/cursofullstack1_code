﻿// C#
using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var nom = "Gandalf";
            SayHello(nom);
            SayHello();

            var suma = suma(5, 76);
            Console.WriteLine($"la suma és{suma}");
        }

        public static void SayHello(string nom)
        {
            Console.WriteLine("Hola " + nom);
            Console.WriteLine($"Hola {nom}");
            Console.WriteLine(string.Format("Hola {0}", nom));
        }

        public static int Sum(int num1, int num2)
        {
            var suma = num1 + num2;
            return suma;
        }

        public static void SayHello()
        {
            Console.WriteLine("Hola");
        }

    }
}