﻿// C#
using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class Program
    {
        static string EscapeWord = "RUNYOUFOOLS";

        static void Main(string[] args)
        {
            Console.WriteLine("Benvinguts al programa per a la gestió d'alumnes");
            Console.WriteLine("Introdueixi les notes dels alumnes");

            var notasAlumnes = new List<double>();

            var keepdoing = true;

            while (keepdoing)
            {
                Console.WriteLine($"Nota de l'alumne {notasAlumnes.Count + 1}:");
                var notaText = Console.ReadLine();

                if (notaText == EscapeWord)
                {
                    keepdoing = false;
                }
                else
                {
                    var nota = 0.0;

                    if (double.TryParse(notaText, out nota))
                    {
                        notasAlumnes.Add(nota);
                    }
                    else
                    {
                        Console.WriteLine("La nota introduïda és incorrecta meló!");
                    }
                }
            }

            var suma = 0.0;
            for (var i = 0; i < notasAlumnes.Count; i++)
            {
                suma += notasAlumnes[i];
            }

            var mitjana = suma / notasAlumnes.Count;
            Console.WriteLine("La mitjana dels examenes és: {0}", mitjana);
        }

    }
}