﻿using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class Program
    {
        static List<double> Marks { get; set; }
        static string EscapeWord = "RUNYOUFOOLS";

        static void Main(string[] args)
        {
            Console.WriteLine("Benvinguts al programa per a la gestió d'alumnes");
            Console.WriteLine("Per introduir notes dels alumnes usi l'opció a");
            Console.WriteLine("Per obtenir estadístiques usi l'opció e");

            Marks = new List<double>();

            var notasDeAlumnos = new List<double>();

            var keepdoing = true;

            while (keepdoing)
            {
                var option = Console.ReadKey().KeyChar;

                if (option == 'a')
                {

                    ShowStudentsMarksMenu();
                }
                else if (option == 'e')
                {
                    ShowStatsMenu();
                }
            }
            ShowMainMenu();
        }

        static void ShowMainMenu()
        {

            Console.WriteLine("Tornar al menú principal");
            Console.WriteLine("Per introduir notes dels alumnes usi l'opció a");
            Console.WriteLine("Per obtenir estadístiques usi l'opció e");
        }

        static void ShowStudentsMarksMenu()
        {
            Console.WriteLine();
            Console.WriteLine("--Menu de Notes--");

            Console.WriteLine("Per veure totes les notes escriu all");
            Console.WriteLine("Per afegir una nova nota escriu la nota");
            Console.WriteLine("Per tornar al menú principal escriu m");

            var keepdoing = true;

            while (keepdoing)
            {
                var text = Console.ReadLine();

                switch (text)
                {
                    case "all":
                        ShowAllStudentsMarks();
                        break;
                    case "m":
                        keepdoing = false;
                        break;
                    default:
                        AddMark(text);
                        break;
                }
            }
            ShowMainMenu();
        }

        static void ShowAllStudentsMarks()
        {
            foreach (var mark in Marks)
            {
                Console.WriteLine(mark);
            }
        }

        static void AddMark(string text)
        {
            var mark = 0.0;

            if (double.TryParse(text, out mark))
            {
                Marks.Add(mark);
                Console.WriteLine("Nota introduida correctament, afegeixi un altre nota més, escrigui all per llistar totes les notes o escrigui m per tornar al menú principal");
            }
            else
            {
                Console.WriteLine($"La nota introduida {text} no está en un format correcte, torni a introduirla correctament");
            }
        }

        static void ShowStatsMenu()
        {
            Console.WriteLine();
            Console.WriteLine("--Menu de Estadístiques--");

            Console.WriteLine("Per veure la mitja escrigui avg");
            Console.WriteLine("Per veure la nota més alta escrigui max");
            Console.WriteLine("Per veure la nota més baja escrigui min");
            Console.WriteLine("Per tornar al menú principal escrigui m");

            var keepdoing = true;

            while (keepdoing)
            {
                var text = Console.ReadLine();

                switch (text)
                {
                    case "avg":
                        ShowAverage();
                        break;
                    case "max":
                        ShowMaximum();
                        break;
                    case "min":
                        ShowMinimum();
                        break;
                    case "m":
                        keepdoing = false;
                        break;
                    default:
                        Console.WriteLine("Comandament no reconegut, introdueix una opció vàlida");
                        break;
                }
            }
            ShowMainMenu();
        }

        static void ShowAverage()
        {
            var suma = 0.0;
            for (var i = 0; i < Marks.Count; i++)
            {
                suma += Marks[i];
            }

            var average = suma / Marks.Count;
            Console.WriteLine("La mitja dels examens és: {0}", average);
        }

        static void ShowMaximum()
        {
            var max = 0.0;
            for (var i = 0; i < Marks.Count; i++)
            {
                if (Marks[i] > max)
                {
                    max = Marks[i];
                }
            }

            Console.WriteLine("La nota més alta és: {0}", max);
        }

        static void ShowMinimum()
        {
            var min = Marks.Count == 0 ? 0.0 : Marks[0];

            for (var i = 0; i < Marks.Count; i++)
            {
                if (Marks[i] < min)
                {
                    min = Marks[i];
                }
            }
            Console.WriteLine("La nota més baja és: {0}", min);
        }
    }
}