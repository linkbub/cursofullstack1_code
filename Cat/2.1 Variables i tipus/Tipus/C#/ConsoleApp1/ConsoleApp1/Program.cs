﻿// C#
using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Escrigui un número");
            var inputText = Console.ReadLine();

            try
            {
                var num1 = int.Parse(inputText);
            }
            catch (System.Exception e1)
            {
                Console.WriteLine($"El format de número[{inputText}] no és correcte");
            }

            int num2;
            bool conversionWasOk = int.TryParse(inputText, out num2);

            if (conversionWasOk)
                Console.WriteLine($"El número[{num2}] es va convertir corréctamente");
            else
                Console.WriteLine($"El format de número[{inputText}] no és correcte");
        }

        static void Declaration1()
        {
            int num1 = 5;
            double num2 = 5.0;

            bool condition1 = true;
            bool condition2 = false;

            string text = "poma";
            char c1 = 'p';

            DateTime data = new DateTime();
            DateTime fechaDeHoy = DateTime.Now;
        }

        static void Declaration2()
        {
            var num1 = 5;
            var num2 = 5.0;

            var condition1 = true;
            var condition2 = false;

            var text = "poma";
            var c1 = 'p';

            var data = new DateTime();
            var fechaDeHoy = DateTime.Now;
        }
    }
}