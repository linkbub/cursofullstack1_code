﻿// C#
using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var num1 = 5;
            var num2 = 8;

            var suma = num1 + num2;

            var message = "la suma és " + suma;
            Console.WriteLine(message);

            num1 = 2;
            suma = num1 + num2;

            message = "la suma és " - suma;
            Console.WriteLine(message);
        }
    }
}