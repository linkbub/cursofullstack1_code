﻿// C#
using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var notaPepe = 10.00;
            var notaCris = 9.99;

            var n = 20;

            var notes = new double[n];

            notes[0] = 10.0;
            notes[1] = 9.99;

            notes[n - 1] = 5;

        }
    }
}