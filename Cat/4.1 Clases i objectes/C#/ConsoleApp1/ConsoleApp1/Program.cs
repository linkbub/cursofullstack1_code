﻿// C#
using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            // forma 1
            var pepe = new Persona();
            pepe.Name = "Pepe";
            pepe.Year = 1978;
            pepe.Email = "a@a.com";

            // forma 2
            var lolo = new Persona("lolo", 2019, "b@b");

            // forma 3
            var maria = new Persona
            {
                Name = "María",
                Year = 1987,
                Email = "m@m.com"
            };

            // forma 1 i 3 són iguals

            ShowPersonaInformation(pepe);
            ShowPersonaInformation(lolo);
            ShowPersonaInformation(maria);

            Console.WriteLine();

            pepe.ShowInfo();
            lolo.ShowInfo();
            maria.ShowInfo();
        }

        // això d'aquí és una funció
        static void ShowPersonaInformation(Persona persona)
        {
            Console.WriteLine("funció estàtica: la persona amb nom " + persona.name
                + " que va néixer l'any del senyor " + persona.Year
                + " i que té d'email " + persona.Email);
        }
    }

    class Persona
    {
        public string Name { get; set; }

        public int Year { get; set; }

        public string Email { get; set; }

        public Persona()
        {

        }

        public Persona(string name, int year, string email)
        {
            this.Name = name;
            this.Year = year;
            this.Email = email;
        }


        // això és un mètode
        public void ShowInfo()
        {
            Console.WriteLine("mètode: la persona amb nom " + this.Name
                    + " que va néixer l'any del senyor " + this.Year
                    + " i que té d'email " + this.Email);
        }

        // això és un overload (sobrecàrrega) del mètode anterior
        public void ShowInfo(bool keepSecrets)
        {

        }
    }
}