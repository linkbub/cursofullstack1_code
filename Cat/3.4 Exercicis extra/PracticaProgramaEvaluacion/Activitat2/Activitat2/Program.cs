using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Activitat2
{
    class Program
    {
        static void Main(string[] args)
        {
            double puntsObtingutsPerPunt = 0;
            double puntsObtingutsPerActivitat = 0;
            double puntsObtingutsTotals = 0;
            int resposta;
            bool seguirActivitat = true;
            bool seguirSubActivitat = true;
            string nomActivitat = "";
            string nomSubActivitat = "";
            

            Console.Write("Introdueix el nom de la practica a evaluar: \n");
            string nomPractica = Console.ReadLine();

            while (seguirActivitat)
            {
                Console.Write("Introdueix el nom de l'activitat: \n");
                nomActivitat = Console.ReadLine();

                while (seguirSubActivitat)
                {
                    Console.Write("Introdueix el punt de l'activitat: \n");
                    nomSubActivitat = Console.ReadLine();

                    Console.Write("Voleu seguir afegint punts a l'activitat? (No:0 / Si:1) \n");
                    resposta = int.Parse(Console.ReadLine());
                }
                Console.Write("Voleu seguir afegint activitats? (No:0 / Si:1) \n");
                resposta = int.Parse(Console.ReadLine());
            }

            Console.Read();
        }
    }
}
