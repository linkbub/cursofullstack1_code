using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Activitat3
{
    class Program
    {
        const int traspasCada = 4;

        static void Main(string[] args)
        {
            int anyPrincipal = 1948;
            int anyNaixement = 1989;
            string anyCert = "L'any de naixement és de traspàs";
            string anyFals = "L'any de naixement no és de traspàs";
            bool verificarAny = false;

            for (int i = anyPrincipal; i < anyNaixement; i += traspasCada)
            {
                Console.Write(i + "\n");
                if (anyNaixement == anyPrincipal)
                {
                    verificarAny = true;
                }
            };

            if (verificarAny == true)
            {
                Console.Write(anyCert);
            }
            else
            {
                Console.Write(anyFals);
            }

            Console.Read();
        }
    }
}
