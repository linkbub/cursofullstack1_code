using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Actividad1
{
    class Program
    {
        static void Main(string[] args)
        {
            string nom = "Leo";
            string cognom1 = "Suarez";
            string cognom2 = "Puyol";

            int dia = 25;
            int mes = 06;
            int any = 1989;

            Console.Write(cognom1 + " " + cognom2 + ", " + nom + "\n");
            Console.Write(dia + "/" + mes + "/" + any);

            Console.ReadLine();
        }
    }
}
