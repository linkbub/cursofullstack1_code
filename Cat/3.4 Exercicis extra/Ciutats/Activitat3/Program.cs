using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Activitat3
{
    class Program
    {
        const int maxCiutats = 6;

        static void Main(string[] args)
        {
            string bcn = "";
            string mdrd = "";
            string vlc = "";
            string mlg = "";
            string cad = "";
            string sant = "";

            string[] arrayCiutatsModificades = new string[maxCiutats];

            Console.WriteLine("Introdueix un nom de ciutat:");
            bcn = Console.ReadLine();
            Console.WriteLine("Introdueix un nom de ciutat:");
            mdrd = Console.ReadLine();
            Console.WriteLine("Introdueix un nom de ciutat:");
            vlc = Console.ReadLine();
            Console.WriteLine("Introdueix un nom de ciutat:");
            mlg = Console.ReadLine();
            Console.WriteLine("Introdueix un nom de ciutat:");
            cad = Console.ReadLine();
            Console.WriteLine("Introdueix un nom de ciutat:");
            sant = Console.ReadLine();
            Console.Write("\n");

            arrayCiutatsModificades[0] = bcn.Replace('a', '4');
            arrayCiutatsModificades[1] = mdrd.Replace('a', '4');
            arrayCiutatsModificades[2] = vlc.Replace('a', '4');
            arrayCiutatsModificades[3] = mlg.Replace('a', '4');
            arrayCiutatsModificades[4] = cad.Replace('a', '4');
            arrayCiutatsModificades[5] = sant.Replace('a', '4');

            Array.Sort(arrayCiutatsModificades);

            for (int i = 0; i < maxCiutats; i++)
            {
                Console.WriteLine(arrayCiutatsModificades[i]);
            }

            Console.Read();
        }
    }
}
