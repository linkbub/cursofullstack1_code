using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Activitat3
{
    class Program
    {
        static void Main(string[] args)
        {
            char[] lletresNom = { 'n', 'o', 'm' };
            List<char> llistaNom = new List<char>();

            for (int i = 0; i < lletresNom.Length; i++)
            {
                llistaNom.Add(lletresNom[i]);
            }

            Dictionary<char, int> diccionariNom = new Dictionary<char, int>();

            foreach (var nom in llistaNom)
            {
                if (!diccionariNom.ContainsKey(nom))
                    diccionariNom.Add(nom, 1);
                else
                {
                    diccionariNom[nom]++;
                }
            }

            foreach (var nom in diccionariNom)
            {
                Console.WriteLine(nom);
            }
            Console.Read();
        }
    }
}
