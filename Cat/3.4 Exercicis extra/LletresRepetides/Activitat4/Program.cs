using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Activitat4
{
    class Program
    {
        static void Main(string[] args)
        {
            char[] lletresNom = { 'n', 'o', 'm' };
            char[] lletresCognom = { 'c', 'o', 'g', 'n', 'o', 'm' };

            List<char> llistaNom = new List<char>();
            List<char> llistaCognom = new List<char>();
            List<char> llistaNomComplet = new List<char>();

            for (int i = 0; i < lletresNom.Length; i++)
            {
                llistaNom.Add(lletresNom[i]);
            }

            for (int i = 0; i < lletresCognom.Length; i++)
            {
                llistaCognom.Add(lletresCognom[i]);
            }

            llistaNomComplet.AddRange(llistaNom);
            llistaNomComplet.Add(' ');
            llistaNomComplet.AddRange(llistaCognom);

            foreach (var letra in llistaNomComplet)
            {
                Console.Write(letra);
            }

            Console.Read();
        }
    }
}
