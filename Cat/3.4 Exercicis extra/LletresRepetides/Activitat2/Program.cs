using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Activitat2
{
    class Program
    {
        static void Main(string[] args)
        {
            char[] lletresNom = { 'n', 'o', 'm' };
            List<char> llistaNom = new List<char>();

            for (int i = 0; i < lletresNom.Length; i++)
            {
                llistaNom.Add(lletresNom[i]);
            }

            foreach (var nom in llistaNom)
            {
                switch (nom)
                {
                    case 'a':
                    case 'e':
                    case 'i':
                    case 'o':
                    case 'u':
                        Console.WriteLine($"La lletra {nom} és vocal");
                        break;

                    case '0':
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                        Console.WriteLine($"La lletra {nom} és un numero i els noms no contenen números!");
                        break;
                    default:
                        Console.WriteLine($"La lletra {nom} és consonant");
                        break;
                }
            }
            Console.Read();
        }
    }
}
