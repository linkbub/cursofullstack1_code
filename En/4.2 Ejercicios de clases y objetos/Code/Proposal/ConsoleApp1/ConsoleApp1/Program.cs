﻿using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class Program
    {
        static string EscapeWorld = "RUNYOUFOOLS";

        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to our Students Manager Application");
            Console.WriteLine("Please type marks for the student and use the escape world RUNYOUFOOLS in order to show the average");

            var studentsMarks = new List<double>();
            var keepdoing = true;

            while(keepdoing)
            {
                Console.WriteLine($"Marks for student {studentsMarks.Count + 1}:");

                var markText = Console.ReadLine();

                if (markText == EscapeWorld)
                {
                    keepdoing = false;
                }
                else
                {
                    var mark = 0.0;

                    if (double.TryParse(markText, out mark))
                    {
                        studentsMarks.Add(mark);
                    }
                    else
                    {
                        Console.WriteLine("hey dude, the mark was not in a correct format, wtf!");
                    }
                }
            }

            var sum = 0.0;

            for (var i = 0; i < studentsMarks.Count; i++)
            {
                sum += studentsMarks[i];
            }

            var average = sum / studentsMarks.Count;
            Console.WriteLine($"The average for all exams marks is: {average}");
        }
    }
}
