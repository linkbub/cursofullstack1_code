﻿using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class Program
    {
        static string EscapeWorld = "RUNYOUFOOLS";
        static List<double> StudentsMarks { get; set; } = new List<double>();

        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to our Students Manager Application");
            Console.WriteLine("To see students marks menu press a");
            Console.WriteLine("To see stats menu press e");

            var keepdoing = true;

            while (keepdoing)
            {
                var option = Console.ReadKey().KeyChar;

                if (option == 'a')
                {
                    ShowStudentsMenu();
                }
                else if (option == 'e')
                {
                    ShowStatsMenu();
                }
            }
        }


        static void ShowStudentsMenu()
        {
            Console.WriteLine();
            Console.WriteLine("-- Marks Menu --");
            Console.WriteLine("To see all students marks menu type all");
            Console.WriteLine("To go to the main menu type m");
            Console.WriteLine("Too add a new mark type it and press enter");

            var keepdoing = true;
            while (keepdoing)
            {
                var text = Console.ReadLine();

                switch (text)
                {
                    case "all":
                        ShowAllStudentsMarks();
                        break;

                    case "m":
                        keepdoing = false;
                        break;

                    default:
                        AddMark(text);
                        break;
                }
            }

            ShowMainMenu();
        }

        private static void ShowMainMenu()
        {
            Console.WriteLine("Came back to Main Menu");
            Console.WriteLine("To see students marks menu press a");
            Console.WriteLine("To see stats menu press e");
        }

        private static void ShowAllStudentsMarks()
        {
            foreach(var mark in StudentsMarks)
            {
                Console.WriteLine(mark);
            }
        }

        private static void AddMark(string text)
        {
            var mark = 0.0;

            if (double.TryParse(text, out mark))
            {
                StudentsMarks.Add(mark);
                Console.WriteLine("The mark was added correctly keep adding more, type all to see all marks, or type m to come back to main menu");
            }
            else
            {
                Console.WriteLine($"the mark {text} was not in a correct format, please type it correctly");
            }
        }

        static void ShowStatsMenu()
        {
            Console.WriteLine();
            Console.WriteLine("-- Stats Menu --");
            Console.WriteLine("To see the average type avg");
            Console.WriteLine("To see the maximum mark type max");
            Console.WriteLine("To see the minimum mark type min");
            Console.WriteLine("To go to the main menu type m");

            var keepdoing = true;
            while (keepdoing)
            {
                var text = Console.ReadLine();

                switch (text)
                {
                    case "avg":
                        ShowAverage();
                        break;
                    case "max":
                        ShowMaximum();
                        break;
                    case "min":
                        ShowMinimum();
                        break;
                    case "m":
                        keepdoing = false;
                        break;

                    default:
                        Console.WriteLine("Please type a valid option or go to main menu typing m");
                        break;
                }
            }

            ShowMainMenu();
        }

        private static void ShowAverage()
        {
            var sum = 0.0;

            for (var i = 0; i < StudentsMarks.Count; i++)
            {
                sum += StudentsMarks[i];
            }

            var average = sum / StudentsMarks.Count;
            Console.WriteLine($"The average for all exams marks is: {average}");
        }

        private static void ShowMaximum()
        {
            var max = 0.0;

            for (var i = 0; i < StudentsMarks.Count; i++)
            {
                if (StudentsMarks[i] > max)
                    max = StudentsMarks[i];
            }

            Console.WriteLine($"The highest mark was: {max}");
        }

        private static void ShowMinimum()
        {
            //var min = 0.0;

            //if (StudentsMarks.Count == 0)
            //    min = 0.0;
            //else
            //    min = StudentsMarks[0];

            var min = StudentsMarks.Count == 0 ? 0.0 : StudentsMarks[0];

            for (var i = 0; i < StudentsMarks.Count; i++)
            {
                if (StudentsMarks[i] < min)
                    min = StudentsMarks[i];
            }

            Console.WriteLine($"The lowest mark was: {min}");
        }
    }


}
