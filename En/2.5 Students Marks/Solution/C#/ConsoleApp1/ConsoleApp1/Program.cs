﻿using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class Program
    {

        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to our Students Manager Application");

            // 1 change the size of the array from 5 to 10 and add some values
            var studentsMarks = new double[10];

            studentsMarks[0] = 4.6;
            studentsMarks[1] = 3.6;
            studentsMarks[2] = 4.9;
            studentsMarks[3] = 2.6;
            studentsMarks[4] = 9.5;
            studentsMarks[5] = 2.6;
            studentsMarks[6] = 3.4;
            studentsMarks[7] = 2.1;
            studentsMarks[8] = 2.66;
            studentsMarks[9] = 8.15;

            // 2 adjust average calculation to use the extra values
            var sum = studentsMarks[0] +
                        studentsMarks[1] +
                        studentsMarks[2] +
                        studentsMarks[3] +
                        studentsMarks[4] +
                        studentsMarks[5] +
                        studentsMarks[6] +
                        studentsMarks[7] +
                        studentsMarks[8] +
                        studentsMarks[9];

            var average = sum / studentsMarks.Length;

            Console.WriteLine($"The average is {average}");

            // 3 show the max value in the array
            var max = studentsMarks[0];

            if (studentsMarks[1] > max)
                max = studentsMarks[1];

            if (studentsMarks[2] > max)
                max = studentsMarks[2];

            if (studentsMarks[3] > max)
                max = studentsMarks[3];

            if (studentsMarks[4] > max)
                max = studentsMarks[4];

            if (studentsMarks[5] > max)
                max = studentsMarks[5];

            if (studentsMarks[6] > max)
                max = studentsMarks[6];

            if (studentsMarks[7] > max)
                max = studentsMarks[7];

            if (studentsMarks[8] > max)
                max = studentsMarks[8];

            if (studentsMarks[8] > max)
                max = studentsMarks[8];

            Console.WriteLine($"The highest mark was {max}");

            // 4 show the min value in the array

            var min = studentsMarks[0];

            if (studentsMarks[1] < min)
                min = studentsMarks[1];

            if (studentsMarks[2] < min)
                min = studentsMarks[2];

            if (studentsMarks[3] < min)
                min = studentsMarks[3];

            if (studentsMarks[4] < min)
                min = studentsMarks[4];

            if (studentsMarks[5] < min)
                min = studentsMarks[5];

            if (studentsMarks[6] < min)
                min = studentsMarks[6];

            if (studentsMarks[7] < min)
                min = studentsMarks[7];

            if (studentsMarks[8] < min)
                min = studentsMarks[8];

            if (studentsMarks[8] < min)
                min = studentsMarks[8];

            Console.WriteLine($"The lowest mark was {min}");
        }
    }
}
