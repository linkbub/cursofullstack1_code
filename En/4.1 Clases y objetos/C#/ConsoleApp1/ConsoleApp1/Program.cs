﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            //ShowPersonInformation("Pepe", 1234, "a@a.com");

            // way 1
            var pepe = new Person();
            pepe.Name = "Pepe";
            pepe.Year = 1234;
            pepe.Email = "a@a.com";

            // way 2
            var maria = new Person("Maria", 1232, "m@m.com");

            // way 3
            var lolo = new Person
            {
                Name = "Lolo",
                Year = 1224,
                Email = "b@b.com"
            };

            ShowPersonInformation(pepe);

            Console.WriteLine();
            pepe.ShowInformation();
            maria.ShowInformation();
            lolo.ShowInformation();

        }

        // this a function
        static void ShowPersonInformation(Person input)
        {
            Console.WriteLine("static function: the person with name: " + input.Name
                + " that was born in the year " + input.Year
                + " has the email:" + input.Email);
        }

    }

    class Person
    {
        // this is a field
        public string Address;

        // this a property
        public string Name { get; set; }
        public int Year { get; set; }
        public string Email { get; set; }

        public Person()
        {

        }

        public Person(string name, int year, string email)
        {
            this.Name = name;
            this.Year = year;
            this.Email = email;
        }


        // this is a method
        public void ShowInformation()
        {
            Console.WriteLine("class method: the person with name: " + this.Name
                + " that was born in the year " + this.Year
                + " has the email:" + this.Email);
        }

        // this is an overload of the previous method

        public void ShowInformation(bool keepsecret)
        {
            if (keepsecret)
                Console.WriteLine("class method: the person with name: " + this.Name);
            else
                Console.WriteLine("class method: the person with name: " + this.Name
                + " that was born in the year " + this.Year
                + " has the email:" + this.Email);
        }
    }


}
