﻿using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class Program
    {

        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to our Students Manager Application");

            // 1 change the size of the array from 5 to 10 and add some values
            var studentsMarks = new double[5];

            studentsMarks[0] = 4.6;
            studentsMarks[1] = 3.6;
            studentsMarks[2] = 4.9;
            studentsMarks[3] = 2.6;
            studentsMarks[4] = 9.5;

            // 2 adjust average calculation to use the extra values
            var sum = studentsMarks[0] +
                        studentsMarks[1] +
                        studentsMarks[2] +
                        studentsMarks[3] +
                        studentsMarks[4];

            var average = sum / studentsMarks.Length;

            Console.WriteLine($"The average is {average}");

            // 3 show the max value in the array

            // 4 show the min value in the array
        }
    }
}
