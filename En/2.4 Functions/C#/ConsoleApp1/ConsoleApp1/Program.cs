﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            var wizard = "Gandalf";
            var year = 2019;

            var message = "Hello " + wizard + " the year is " + year;

            Console.WriteLine(message);

            var num1 = 5;
            var num2 = 8;

            var sum = num1 + num2;
            Console.WriteLine("the sumatory of " + num1 + " and " + num2 + " is " + sum);

            num1 = 2;
            sum = num1 + num2;
            Console.WriteLine("now the sumatory is " + sum);

            var sub = num1 - num2;
            var mult = num1 * num2;
            var div = num1 / num2;

        }
    }
}
