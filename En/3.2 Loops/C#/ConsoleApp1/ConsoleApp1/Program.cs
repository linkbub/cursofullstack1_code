﻿using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            //Loop1();
            Loop2();
        }

        private static void Loop1()
        {
            var list = new List<string>();
            list.Add("feather");
            list.Add("pizza");
            list.Add("bitcoin");

            for (var i = 0; i < list.Count; i++)
            {
                var word = list[i];

                Console.WriteLine("the word in the position " + i.ToString() + " is " + word);
            }

            var condition = true;
            var j = 0;

            while (condition)
            {
                var word = list[j];

                if (word.Contains("z"))
                {
                    condition = false;
                    Console.WriteLine("we found the ilegal character in the word " + word + " at position " + j);
                }
                else
                {
                    Console.WriteLine("The word in the position " + j + " is" + word);
                    j++;
                }
            }
        }

        private static void Loop2()
        {
            var list = new List<string>();

            var keepdoing = true;

            while (keepdoing)
            {
                Console.WriteLine("Please type a word or type EXIT to leave");
                var word = Console.ReadLine();

                if (word == "EXIT")
                {
                    keepdoing = false;
                }
                else
                {
                    list.Add(word);
                }
            }

            Console.WriteLine("We print the words using loop for");
            for (var i = 0; i < list.Count; i++)
            {
                Console.WriteLine(list[i]);
            }

            Console.WriteLine("we print the words using loop foreach");
            foreach(var word in list)
            {
                Console.WriteLine(word);
            }

        }
    }
}
