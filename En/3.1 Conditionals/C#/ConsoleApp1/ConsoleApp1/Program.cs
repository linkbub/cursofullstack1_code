﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            var come = 0;
            var conditionLeave = come == 2;

            if (come == 0)
            {
                Console.WriteLine("Hello!");
            }
            else if( come == 1)
            {
                Console.WriteLine("are you staying or not?");
            }
            else if (conditionLeave)
            {
                Console.WriteLine("good bye");
            }
            else
            {
                Console.WriteLine("decide!");
            }

            switch (come)
            {
                case 0:
                    Console.WriteLine("Hello!");
                    break;
                case 1:
                    Console.WriteLine("are you staying or not?");
                    break;
                case 2:
                    Console.WriteLine("good bye");
                    break;
                default:
                    Console.WriteLine("decide!");
                    break;
            }
        }
    }
}
