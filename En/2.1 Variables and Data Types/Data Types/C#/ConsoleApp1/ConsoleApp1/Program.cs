﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Write a number");
            var inputText = Console.ReadLine();

            try
            {
                var num1 = int.Parse(inputText);
                var num2 = double.Parse(inputText);
                var condition = bool.Parse(inputText);

                char c1 = inputText[0];

                Console.WriteLine($"The format of the number [{inputText}] was not correct");
            }
            catch (Exception e1)
            {
                Console.WriteLine($"The format of the number [{inputText}] is not correct");
            }

            int num3;
            bool conversionWasOk = int.TryParse(inputText, out num3);

            if (conversionWasOk == true)
                Console.WriteLine($"The number[{num3}] was converted correctly");
            else
                Console.WriteLine($"The format of the number [{inputText}] is not correct");

            Console.WriteLine("The program is going to end");

        }

        private static void ExplicitDeclarations()
        {
            int num1 = 5;
            double num2 = 5.0;
            bool condition1 = true;
            bool condition2 = false;

            string text = "pomelo";
            char c1 = 'p';

            DateTime date = new DateTime();
            DateTime dateToday = DateTime.Now;
        }

        private static void VarDeclarations()
        {
            var num1 = 5;
            var num2 = 5.0;
            var condition1 = true;
            var condition2 = false;

            var text = "pomelo";
            var c1 = 'p';

            var date = new DateTime();
            var dateToday = DateTime.Now;
        }
    }
}
