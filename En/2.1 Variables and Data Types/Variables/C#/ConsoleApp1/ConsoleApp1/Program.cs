﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            SayHello();
            
            var name = "Gandalf";

            SayHello();
            SayHello(name);
            
            var sum = Sum(5, 9);
            console.log("the sumatory is:" + sum);
        }     
        
        public static void SayHello()
        {
            Console.WriteLine("hello!");
        }

        public static void SayHello(string name)
        {
            Console.WriteLine("hello " + name);
            Console.WriteLine(string.Format("hello {0}", name));
            Console.WriteLine($"hello {name}");
        }

        public static int Sum(int num1, int num2)
        {
            var sum = num1 + num2;
            return sum;
        }
    }
}
