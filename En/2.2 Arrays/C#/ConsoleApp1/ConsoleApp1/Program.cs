﻿using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var mark1 = 10.00;
            var mark2 = 5.00;
            var mark3 = 6.00;

            var n = 20;

            var marks = new double[n];
            var texts = new string[50];

            marks[0] = 10.00;
            marks[1] = 5;
            
            marks[n - 1] = 50.00;
            marks[n ] = 50.00;

            var list = new List<double>();
            list.Add(10.0);
            list.Add(5.00);
        }             
    }
}
