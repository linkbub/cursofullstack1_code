﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var num1 = 6;
            var num2 = 9;

            var sum = num1 + num2;
            var sub = num1 - num2;
            var mult = num1 * num2;
            var div = num1 / num2;

            var mod = num1 % num2;

            var pol = num1 * num2 + 5;
            var pol2 = (4 + num1) / 3;

            var helloMsg = "hello ";
            var name = "Gandalf";

            var text = helloMsg + name;
            //var text2 = helloMsg - name;

            //num1 = num1 + 3; //after the instruction we will have 9
            //num1 += 3; //after the instruction we will have 9
            num1 -= 3; //after the instruction we will have 3

            var i = 0;
            var b = i++;
            var c = i--;

        }
    }
}
