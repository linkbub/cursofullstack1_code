﻿using ConsoleApp1.Lib.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApp1
{    
    /// <summary>
     /// El objetivo de esta iteración es usar clases para definir:
     /// Students (Estudiante)
     /// Subjects (Materias)
     /// Exams (Exámenes)
     /// y añadir opciones extras al menú para poder manejarlas
     /// En el ejemplo crearemos las entidades (clases que definen el dominio -funcionalidad- de nuestro programa)
     /// y la opción de menú para añadir estudiantes
     /// El alumno debe completar el resto de funcionalidades CRUD (Create, Read, Update, Destroy)
     /// </summary>
    class Program
    {
        //static List<double> Marks { get; set; }
        public static Dictionary<CrudOptionsTypes, string> CrudOptionsNames = new Dictionary<CrudOptionsTypes, string>
        {
            { CrudOptionsTypes.Add, "add" },
            { CrudOptionsTypes.Edit, "edit" },
            { CrudOptionsTypes.DeleteOrView, "delete" }
        };

        public static Dictionary<Guid, Student> Students = new Dictionary<Guid, Student>();
        public static Dictionary<Guid, Subject> Subjects = new Dictionary<Guid, Subject>();
        public static Dictionary<Guid, Exam> Exams = new Dictionary<Guid, Exam>();


        static string EscapeWord = "RUNYOUFOOLS";

        static void Main(string[] args)
        {
            LoadInitialData();

            Console.WriteLine("Bienvenid@ al programa para gestión de alumnos");
            Console.WriteLine("Para ir a la gestión de alumnos use la opción a");
            Console.WriteLine("Para ir a la gestión de asignaturas use la opción s");
            Console.WriteLine("Para ir a la gestión de alumnos use la opción x");
            Console.WriteLine("Para obtener estadísticas use la opción e");

            //Marks = new List<double>();
            var keepdoing = true;

            while (keepdoing)
            {
                var option = Console.ReadKey().KeyChar;

                if (option == 'a')
                {
                    ShowStudentsMenu();
                }
                else if (option == 's')
                {
                    ShowSubjectsMenu();
                }
                else if (option == 'x')
                {
                    ShowExamsMenu();
                }
                else if(option == 'e')
                {
                    ShowStatsMenu();
                }
            }

            
        }
        
        private static void ShowMainMenu()
        {
            Console.WriteLine("Vuelta al menú principal");
            Console.WriteLine("Para ir a la gestión de alumnos use la opción a");
            Console.WriteLine("Para ir a la gestión de asignaturas use la opción s");
            Console.WriteLine("Para ir a la gestión de alumnos use la opción x");
            Console.WriteLine("Para obtener estadísticas use la opción e");
        }

        #region Student Menu


        static void ShowStudentsMenu()
        {
            Console.WriteLine();
            ShowStudentsMenuOptions();

            var keepdoing = true;
            while (keepdoing)
            {
                var optionText = Console.ReadLine();
                string dni = GetDniForOption(ref optionText);

                switch (optionText)
                {
                    case "all":
                        ShowAllStudents();
                        break;

                    case "add":
                        AddNewStudent();
                        break;

                    case "edit":
                        EditStudent(dni);
                        break;

                    case "delete":
                        DeleteStudent(dni);
                        break;

                    case "m":
                        keepdoing = false;
                        break;

                    default:
                        Console.WriteLine("Opción no válida, escriba otra o m para salir al menú principal");
                        break;
                }
            }

            ShowMainMenu();
        }

        private static void ShowStudentsMenuOptions()
        {
            Console.WriteLine("--Menu de Estudiantes--");

            Console.WriteLine("Para ver todos los estudiantes escriba all");
            Console.WriteLine("Para añadir un nuevo estudiante escriba add");
            Console.WriteLine("Para editar un estudiante escriba edit + el dni");
            Console.WriteLine("Para borrar un estudiante escriba del + el dni");
            Console.WriteLine("Para volver al menú principal escriba m");
        }

        static string GetCrudOptionForDni(CrudOptionsTypes option, string input, out string textOption)
        {
            textOption = string.Empty;

            var optionName = CrudOptionsNames[option];

            if (string.IsNullOrEmpty(input))
                return null;

            if (input.StartsWith(optionName))
            {
                char[] c1 = { ' ' };
                var spaso = input.Split(c1);

                if (spaso.Length > 2)
                    Console.WriteLine("warning: there more parameters than needed after dni");
                else if (spaso.Length > 1)
                {
                    var text = spaso[1];
                    var currentDni = Student.IsDniValid(text) ? text : string.Empty;
                    while (true)
                    {
                        if (!string.IsNullOrEmpty(currentDni))
                        {
                            textOption = optionName;
                            return currentDni;
                        }

                        Console.WriteLine($"El dni {spaso[1]} no existe o tiene un formato incorrecto, vuelva a escribirlo o escriba anular para salir");
                        text = Console.ReadLine();
                        if (text == "anular")
                        {
                            ShowStudentsMenuOptions();
                            return null;
                        }

                        currentDni = Student.IsDniValid(text) ? text : string.Empty;
                    }
                }
            }
            return null;
        }
        
        private static string GetDniForOption(ref string text)
        {
            var prev = text;
            var dni = GetCrudOptionForDni(CrudOptionsTypes.Edit, text, out text);

            if (string.IsNullOrEmpty(dni))
                dni = GetCrudOptionForDni(CrudOptionsTypes.DeleteOrView, text, out text);

            if (string.IsNullOrEmpty(text))
                text = prev;

            return dni;
        }

        static string GetDniFromInput(CrudOptionsTypes option, string currentDni = "")
        {
            Console.WriteLine("Introduzca el dni o anular para salir");
            var text = Console.ReadLine();
            var optionName = CrudOptionsNames[option];

            while (true)
            {
                if (text == "anular")
                    return null;

                if (!Student.ValidateDniFormat(text))
                {
                    Console.WriteLine(Student.DniFormatError);
                    Console.WriteLine("Introduzca el dni o anular para salir");
                }
                else if (!Student.ValidateDniDuplicated(text, currentDni)
                        && (optionName != CrudOptionsNames[CrudOptionsTypes.DeleteOrView]))
                {
                    Console.WriteLine($"{Student.DniDuplicated} {text}");
                    Console.WriteLine("Introduzca el dni o anular para salir");
                }
                else
                {
                    return text;
                }

                text = Console.ReadLine();
            }

        }

        static string GetNameFromInput()
        {
            Console.WriteLine("Introduzca el nombre o anular para salir");
            while (true)
            {
                var input = Console.ReadLine();
                if (input == "anular")
                    return null;

                if (!Student.ValidateNameFormat(input))
                {
                    Console.WriteLine(Student.NameFormatError);
                    Console.WriteLine("Introduzca el nombre o anular para salir");
                }
                else
                {
                    return input;
                }
            }
        }

        #endregion

        #region Subjects Menu

        static void ShowSubjectsMenu()
        {
            Console.WriteLine();
            ShowSubjectsMenuOptions();

            var keepdoing = true;
            while (keepdoing)
            {
                var optionText = Console.ReadLine();
                string name = GetSubjectNameForOption(ref optionText);

                switch (optionText)
                {
                    case "all":
                        ShowAllSubjects();
                        break;

                    case "add":
                        AddNewSubject();
                        break;

                    case "edit":
                        EditSubject(name);
                        break;

                    case "delete":
                        DeleteSubject(name);
                        break;

                    case "m":
                        keepdoing = false;
                        break;

                    default:
                        Console.WriteLine("Opción no válida, escriba otra o m para salir al menú principal");
                        break;
                }
            }

            ShowMainMenu();
        }


        private static void ShowSubjectsMenuOptions()
        {
            Console.WriteLine("--Menu de Subjects--");

            Console.WriteLine("Para ver todos las materias escriba all");
            Console.WriteLine("Para añadir una nueva materia escriba add");
            Console.WriteLine("Para editar una materia escriba edit + el nombre");
            Console.WriteLine("Para borrar una materia escriba del + el nombre");
            Console.WriteLine("Para volver al menú principal escriba m");
        }

        static string GetCrudOptionForSubjectName(CrudOptionsTypes option, string input, out string textOption)
        {
            textOption = string.Empty;

            var optionName = CrudOptionsNames[option];

            if (string.IsNullOrEmpty(input))
                return null;

            if (input.StartsWith(optionName))
            {
                char[] c1 = { ' ' };
                var spaso = input.Split(c1);

                if (spaso.Length > 2)
                    Console.WriteLine("warning: there more parameters than needed after subject name");
                else if (spaso.Length > 1)
                {
                    var text = spaso[1];
                    var currentName = Subject.IsNameValid(text) ? text : string.Empty;
                    while (true)
                    {
                        if (!string.IsNullOrEmpty(currentName))
                        {
                            textOption = optionName;
                            return currentName;
                        }

                        Console.WriteLine($"La asignatura {spaso[1]} no existe o tiene un formato incorrecto, vuelva a escribirla o escriba anular para salir");
                        text = Console.ReadLine();
                        if (text == "anular")
                        {
                            ShowStudentsMenuOptions();
                            return null;
                        }

                        currentName = Subject.IsNameValid(text) ? text : string.Empty;
                    }
                }
            }
            return null;
        }

        static string GetSubjectNameForOption(ref string text)
        {
            var prev = text;
            var name = GetCrudOptionForSubjectName(CrudOptionsTypes.Edit, text, out text);

            if (string.IsNullOrEmpty(name))
            {
                text = prev;
                name = GetCrudOptionForSubjectName(CrudOptionsTypes.DeleteOrView, text, out text);
            }

            if (string.IsNullOrEmpty(text))
                text = prev;

            return name;
        }

        static string GetSubjectNameFromInput(CrudOptionsTypes option, string currentName = "")
        {
            Console.WriteLine("Introduzca el nombre de la asignatura o anular para salir");
            var text = Console.ReadLine();
            var optionName = CrudOptionsNames[option];

            while (true)
            {
                if (text == "anular")
                    return null;

                if (!Subject.ValidateNameFormat(text))
                {
                    Console.WriteLine(Subject.NameFormatError);
                    Console.WriteLine("Introduzca el nombre o anular para salir");
                }
                else if (!Subject.ValidateNameDuplicated(text, currentName)
                        && (optionName != CrudOptionsNames[CrudOptionsTypes.DeleteOrView]))
                {
                    Console.WriteLine($"{Subject.NameDuplicated} {text}");
                    Console.WriteLine("Introduzca el nombre o anular para salir");
                }
                else
                {
                    return text;
                }

                text = Console.ReadLine();
            }

        }

        #endregion

        #region Exams Menu

        static void ShowExamsMenu()
        {
            Console.WriteLine();
            ShowExamsMenuOptions();
            char[] c1 = { ' ' };

            var keepdoing = true;
            while (keepdoing)
            {
                var optionText = Console.ReadLine();
                var extraOption = string.Empty;

                if (optionText.Contains("allby"))
                {
                    var spaso = optionText.Split(c1);
                    if (spaso.Length > 0)
                    {
                        optionText = spaso[0];
                        if (spaso.Length > 1)
                            extraOption = spaso[1];
                    }
                }


                switch (optionText)
                {
                    case "all":
                        ShowAllExams();
                        break;

                    case "allbystudent":
                        ShowAllExamsByStudentDni(extraOption);
                        break;

                    case "allbysubject":
                        ShowAllExamsBySubjectName(extraOption); 
                        break;

                    case "add":
                        AddNewExam();
                        break;

                    case "m":
                        keepdoing = false;
                        break;

                    default:
                        Console.WriteLine("Opción no válida, escriba otra o m para salir al menú principal");
                        break;
                }
            }

            ShowMainMenu();
        }


        private static void ShowExamsMenuOptions()
        {
            Console.WriteLine("--Menu de Exámenes--");

            Console.WriteLine("Para ver todos los resultados escriba all");
            Console.WriteLine("Para ver todos los resultados escriba allbystudent");
            Console.WriteLine("Para ver todos los resultados escriba allbysubject");
            Console.WriteLine("Para añadir una nueva nota de un examen escriba add");
            Console.WriteLine("Para volver al menú principal escriba m");
        }


        static double GetMarkFromInput()
        {
            Console.WriteLine("Introduzca la nota o anular para salir");
            while (true)
            {
                var input = Console.ReadLine();
                if (input == "anular")
                    return -1;
                
                if (double.TryParse(input, out double mark))
                {
                    if (Exam.ValidateMarkRange(mark))
                    {
                        return mark;
                    }
                    else
                    {
                        Console.WriteLine(Exam.MarkRangeError);
                    }
                }
                else
                {
                    Console.WriteLine(Exam.MarkFormatError);
                }

                Console.WriteLine("Introduzca el nombre o anular para salir");
                
            }
        }


        #endregion

        #region Student CRUD

        static void ShowAllStudents()
        {
            foreach (var student in Students.Values)
            {
                Console.WriteLine($"{student.Dni} {student.Name}");
            }
        }

        static void AddNewStudent()
        {
            #region Todos los inputs del modelo van a pasar por aquí
            var validatedDni = GetDniFromInput(CrudOptionsTypes.Add);
            if (string.IsNullOrEmpty(validatedDni))
                return;

            var validatedName = GetNameFromInput();
            if (string.IsNullOrEmpty(validatedName))
                return;
            #endregion

            var student = new Student
            {
                Id = Guid.NewGuid(),
                Dni = validatedDni,
                Name = validatedName
            };
            Students.Add(student.Id, student);

            Console.WriteLine($"Student with dni:{validatedDni} and name: {validatedName} successfully added.");

            ShowStudentsMenuOptions();

        }

        private static void EditStudent(string dni)
        {
            #region Todos los inputs del modelo van a pasar por aquí
            var validatedDni = GetDniFromInput(CrudOptionsTypes.Edit, dni);
            if (string.IsNullOrEmpty(validatedDni))
                return;

            var validatedName = GetNameFromInput();
            if (string.IsNullOrEmpty(validatedName))
                return;
            #endregion

            // forma a manijen
            //Student existingStudent = null;
            //foreach(var item in Students)
            //{
            //    // item es un par clave-valor key-value donde value es un objeto de tipo Student
            //    if (item.Value.Dni == dni)
            //    {
            //        existingStudent.Dni = validatedDni;
            //        existingStudent.Name = validatedName;
            //    }
            //}

            // Usando LINQ
            var existingStudent = Students.Values.FirstOrDefault(x => x.Dni == dni);
            if (existingStudent != null)
            {
                existingStudent.Dni = validatedDni;
                existingStudent.Name = validatedName;
                Console.WriteLine("Student updated ok!");
            }
            else
            {

                Console.WriteLine($"Student with dni {dni} not found");
            }
        }

        private static void DeleteStudent(string dni)
        {
            var validatedDni = string.IsNullOrEmpty(dni) ? GetDniFromInput(CrudOptionsTypes.Edit, dni) : dni;
            if (string.IsNullOrEmpty(validatedDni))
                return;
           
            // Usando LINQ
            var existingStudent = Students.Values.FirstOrDefault(x => x.Dni == dni);
            if (existingStudent != null)
            {
                // tenemos que borrarlo de la fuente de datos
                Students.Remove(existingStudent.Id);
                Console.WriteLine("Student sucessfully deleted!");
            }
            else
            {
                Console.WriteLine($"Student with dni {dni} not found");
            }
        }

        #endregion

        #region Subject CRUD

        static void ShowAllSubjects()
        {
            foreach (var subject in Subjects.Values)
            {
                Console.WriteLine($"{subject.Name}");
            }
        }

        static void AddNewSubject()
        {
            #region Todos los inputs del modelo van a pasar por aquí
            var validatedName = GetSubjectNameFromInput(CrudOptionsTypes.Add);
            if (string.IsNullOrEmpty(validatedName))
                return;

            #endregion


            var subjct = new Subject
            {
                Id = Guid.NewGuid(),
                Name = validatedName
            };

            Subjects.Add(subjct.Id, subjct);

            Console.WriteLine($"Subject {validatedName} successfully added.");
            ShowSubjectsMenuOptions();
        }

        private static void EditSubject(string name)
        {
            #region Todos los inputs del modelo van a pasar por aquí

            var validatedName = GetSubjectNameFromInput(CrudOptionsTypes.Edit, name);
            if (string.IsNullOrEmpty(validatedName))
                return;

            #endregion


            // Usando LINQ
            var existingSubject = Subjects.Values.FirstOrDefault(x => x.Name == name);
            if (existingSubject != null)
            {
                existingSubject.Name = validatedName;
                Console.WriteLine("Subject updated ok!");
            }
            else
            {

                Console.WriteLine($"Subject with name {validatedName} not found");
            }
        }

        private static void DeleteSubject(string name)
        {
            var validatedName = string.IsNullOrEmpty(name) ? GetSubjectNameFromInput(CrudOptionsTypes.Edit, name) : name;
            if (string.IsNullOrEmpty(validatedName))
                return;

            // Usando LINQ
            var existingItem = Subjects.Values.FirstOrDefault(x => x.Name == name);
            if (existingItem != null)
            {
                Subjects.Remove(existingItem.Id);
                Console.WriteLine("Subject sucessfully delete!");
            }
            else
            {

                Console.WriteLine($"Subject with name {name} not found");
            }
        }

        #endregion
        
        #region Exams CRUD

        static void ShowAllExams()
        {
            foreach (var exam in Exams.Values)
            {
                Console.WriteLine($"materia: {exam.Subject.Name} alumno: {exam.Student.Name} nota: {exam.Mark}");
            }
        }

        static void ShowAllExamsByStudentDni(string dni)
        {
            foreach (var exam in Exams.Values)
            {
                if (exam.Student.Dni == dni)
                    Console.WriteLine($"materia: {exam.Subject.Name} alumno: {exam.Student.Name} nota: {exam.Mark}");
            }
        }

        static void ShowAllExamsBySubjectName(string name)
        {
            foreach (var exam in Exams.Values)
            {
                if (exam.Subject.Name == name)
                    Console.WriteLine($"materia: {exam.Subject.Name} alumno: {exam.Student.Name} nota: {exam.Mark}");
            }
        }

        static void AddNewExam()
        {
            #region Todos los inputs del modelo van a pasar por aquí
            var validatedDni = GetDniFromInput(CrudOptionsTypes.DeleteOrView);
            if (string.IsNullOrEmpty(validatedDni))
                return;

            var validateSubjectName = GetSubjectNameFromInput(CrudOptionsTypes.DeleteOrView);
            if (string.IsNullOrEmpty(validateSubjectName))
                return;

            var validatedMark = GetMarkFromInput();
            if (validatedMark == -1)
                return;

            #endregion

            var currentStudent = Students.Values.FirstOrDefault(x => x.Dni == validatedDni);

            if (currentStudent == null)
            {
                Console.WriteLine($"No se ha encontrado ningún alumno con dni {validatedDni}");
                ShowExamsMenuOptions();
                return;

            }

            var currentSubject = Subjects.Values.FirstOrDefault(x => x.Name == validateSubjectName);
            if (currentSubject == null)
            {
                Console.WriteLine($"No se ha encontrado ninguna asignatura con nombre {validateSubjectName}");
                ShowExamsMenuOptions();
                return;

            }

            var exam = new Exam
            {
                Id = Guid.NewGuid(),
                Student = currentStudent,
                Subject = currentSubject,
                Mark = validatedMark,
                TimeStamp = DateTime.Now
            };
            Exams.Add(exam.Id, exam);

            Console.WriteLine($"Exam for Student with dni:{validatedDni} and subject: {validateSubjectName} successfully added with grade:{validatedMark}.");

            ShowExamsMenuOptions();

        }

        #endregion

        #region Stats Menu

        static void ShowStatsMenu()
        {
            Console.WriteLine();
            Console.WriteLine("--Menu de Estadísticas--");

            Console.WriteLine("Para ver la media escriba avg");
            Console.WriteLine("Para ver la nota más alta escriba max");
            Console.WriteLine("Para ver la nota más baja escriba min");
            Console.WriteLine("Para volver al menú principal escriba m");

            var keepdoing = true;
            while (keepdoing)
            {
                var text = Console.ReadLine();

                switch (text)
                {
                    case "avg":
                        ShowAverage();
                        break;
                    case "max":
                        ShowMaximum();
                        break;
                    case "min":
                        ShowMinimum();
                        break;
                    case "m":
                        keepdoing = false;
                        break;
                    default:
                        Console.WriteLine("comando no reconocido, introduzca una opción válida");
                        break;
                }
            }
            ShowMainMenu();
        }
        #endregion


        static void ShowAverage()
        {
            var suma = 0.0;

            foreach(var exam in Exams.Values)
            {
                suma += exam.Mark;
            }

            var average = suma / Exams.Values.Count;
            Console.WriteLine("la media los exámenes es: {0}", average);
        }

        static void ShowMaximum()
        {
            var max = 0.0;
            foreach (var exam in Exams.Values)
            {
                if (exam.Mark > max)
                    max = exam.Mark;
            }

            Console.WriteLine("la nota más alta es: {0}", max);
        }

        static void ShowMinimum()
        {
            var exams = Exams.Values.ToArray();
            //var min = 0.0;
            //if (Exams.Values.Count == 0)
            //    min = 0.0;
            //else
            //    min = marks[0].Mark;

            //esto(el operador ternario) hace lo mismo que lo de arriba
            var min = exams.Length == 0 ? 0.0 : exams[0].Mark;

            for (var i = 0; i < exams.Length; i++)
            {
                if (exams[i].Mark < min)
                    min = exams[i].Mark;
            }

            Console.WriteLine("la nota más baja es: {0}", min);
        }

        public static void LoadInitialData()
        {
            var pepe = new Student
            {
                Id = Guid.NewGuid(),
                Dni = "12345678a",
                Name = "pepe"
            };
            var lolo = new Student
            {
                Id = Guid.NewGuid(),
                Dni = "11111111a",
                Name = "lolo"
            };

            Students.Add(pepe.Id, pepe);
            Students.Add(lolo.Id, lolo);

            var subject1 = new Subject
            {
                Id = Guid.NewGuid(),
                Name = "Matemáticas"
            };

            var subject2 = new Subject
            {
                Id = Guid.NewGuid(),
                Name = "Programación"
            };
            
            Subjects.Add(subject1.Id, subject1);
            Subjects.Add(subject2.Id, subject2);

            var exam1_pepe = new Exam()
            {
                Id = Guid.NewGuid(),
                Student = pepe,
                Subject = subject1,
                Mark = 9.0
            };
            var exam2_pepe = new Exam()
            {
                Id = Guid.NewGuid(),
                Student = pepe,
                Subject = subject2,
                Mark = 10.0
            };
            var exam1_lolo = new Exam()
            {
                Id = Guid.NewGuid(),
                Student = lolo,
                Subject = subject1,
                Mark = 7.0
            };
            var exam2_lolo = new Exam()
            {
                Id = Guid.NewGuid(),
                Student = lolo,
                Subject = subject2,
                Mark = 6.6
            };

            Exams.Add(exam1_pepe.Id, exam1_pepe);
            Exams.Add(exam2_pepe.Id, exam2_pepe);
            Exams.Add(exam1_lolo.Id, exam1_lolo);
            Exams.Add(exam2_lolo.Id, exam2_lolo);
        }
    }

    public enum CrudOptionsTypes
    {
        Add,
        Edit,
        DeleteOrView
    }
}
