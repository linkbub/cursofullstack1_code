﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var num1 = 5;
            var num2 = 6;

            var suma = num1 + num2;
            var resta = num1 - num2;
            var multiplicacion = num1 * num2;
            var div = num1 / num2;

            var operacion1 = (5 + 8) / 5 * 7;


            var message = "Hola ";
            var name = "Gandalf";

            var text = message + name;

            //var text2 = message - name; error

            num1 = num1 + 3;
            num1 += 3;
            num1 *= 2;

            var num3 = num1++; //num + 1;
        }       
    }
}
