﻿using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            
            //Loop1();
            Loop2();
        }
               
        private static void Loop1()
        {
            var lista = new List<string>();

            lista.Add("pluma");
            lista.Add("bitcoin");
            lista.Add("pizza");

            for (var i = 0; i < lista.Count; i++)
            {
                var palabra = lista[i];
                Console.WriteLine($"la palabra en la posición {i} es " + palabra);
            }

            var condition = true;
            var j = 0;

            while (condition)
            {
                var palabra = lista[j];

                if (palabra.Contains("y"))
                {
                    condition = false;
                    Console.WriteLine($"hemos encontrado la letra porhibida en la palabra {palabra} en la posición {j}");
                }
                else
                {
                    Console.WriteLine($"la palabra en la posición {j} es " + palabra);
                    j++;
                }
            }
        }

        private static void Loop2()
        {
            var lista = new List<string>();

            var keepdoing = true;

            while (keepdoing)
            {
                Console.WriteLine("Introduzca una palabra y escriba EXIT para salir");
                var word = Console.ReadLine();

                if (word == "EXIT")
                {
                    keepdoing = false;
                }
                else
                {
                    lista.Add(word);
                }
            }

            Console.WriteLine("Imprimimos las palabras usando el bucle for");
            for (var i = 0; i < lista.Count; i++)
            {
                Console.WriteLine(lista[i]);
            }

            Console.WriteLine("Imprimimos las palabras usando el bucle foreach");
            foreach(var word in lista)
            {
                Console.WriteLine(word);
            }
        }
    }
}
