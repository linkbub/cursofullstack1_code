﻿using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var notaDePepe = 10.00;
            var notaDeCris = 9.99;

            var n = 20;

            var notas = new double[n];

            notas[0] = 10.0;
            notas[1] = 9.99;

            notas[n - 1] = 5;

            EjemploDiccionarios();
        }

        private static void EjemploDiccionarios()
        {
            // Esto se explica en el vídeo de diccionarios
            var diccionario = new Dictionary<string, int>();
            diccionario.Add("A1", 100);

            if (!diccionario.ContainsKey("A2"))
            {
                diccionario.Add("A2", 300);
            }
            else
            {
                //esto daría error
                diccionario["A5"] = 500;
            }

            foreach (var par in diccionario)
            {
                Console.WriteLine($"La clave {par.Key} tiene como valor {par.Value}");
            }

            foreach (var valor in diccionario.Values)
            {
                Console.WriteLine($"{valor}");
            }
        }
    }
}
