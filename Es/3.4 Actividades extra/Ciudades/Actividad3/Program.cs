using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Actividad3
{
    class Program
    {
        const int maxCiudades = 6;

        static void Main(string[] args)
        {
            string bcn = "";
            string mdrd = "";
            string vlc = "";
            string mlg = "";
            string cad = "";
            string sant = "";

            string[] arrayCiudadesModificadas = new string[maxCiudades];

            Console.WriteLine("Introduce un nombre de ciudad:");
            bcn = Console.ReadLine();
            Console.WriteLine("Introduce un nombre de ciudad:");
            mdrd = Console.ReadLine();
            Console.WriteLine("Introduce un nombre de ciudad:");
            vlc = Console.ReadLine();
            Console.WriteLine("Introduce un nombre de ciudad:");
            mlg = Console.ReadLine();
            Console.WriteLine("Introduce un nombre de ciudad:");
            cad = Console.ReadLine();
            Console.WriteLine("Introduce un nombre de ciudad:");
            sant = Console.ReadLine();
            Console.Write("\n");

            arrayCiudadesModificadas[0] = bcn.Replace('a', '4');
            arrayCiudadesModificadas[1] = mdrd.Replace('a', '4');
            arrayCiudadesModificadas[2] = vlc.Replace('a', '4');
            arrayCiudadesModificadas[3] = mlg.Replace('a', '4');
            arrayCiudadesModificadas[4] = cad.Replace('a', '4');
            arrayCiudadesModificadas[5] = sant.Replace('a', '4');

            Array.Sort(arrayCiudadesModificadas);

            for (int i = 0; i < maxCiudades; i++)
            {
                Console.WriteLine(arrayCiudadesModificadas[i]);
            }

            Console.Read();
        }
    }
}
