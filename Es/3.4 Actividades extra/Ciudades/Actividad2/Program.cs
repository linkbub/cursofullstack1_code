using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Actividad2
{
    class Program
    {
        const int maxCiudades = 6;

        static void Main(string[] args)
        {
            string bcn = "";
            string mdrd = "";
            string vlc = "";
            string mlg = "";
            string cad = "";
            string sant = "";

            string[] arrayCiudades = new string[maxCiudades];

            Console.WriteLine("Introduce un nombre de ciudad:");
            bcn = Console.ReadLine();
            Console.WriteLine("Introduce un nombre de ciudad:");
            mdrd = Console.ReadLine();
            Console.WriteLine("Introduce un nombre de ciudad:");
            vlc = Console.ReadLine();
            Console.WriteLine("Introduce un nombre de ciudad:");
            mlg = Console.ReadLine();
            Console.WriteLine("Introduce un nombre de ciudad:");
            cad = Console.ReadLine();
            Console.WriteLine("Introduce un nombre de ciudad:");
            sant = Console.ReadLine();

            arrayCiudades[0] = bcn;
            arrayCiudades[1] = mdrd;
            arrayCiudades[2] = vlc;
            arrayCiudades[3] = mlg;
            arrayCiudades[4] = cad;
            arrayCiudades[5] = sant;

            Array.Sort(arrayCiudades);
            Console.WriteLine("\n Ciudades ordenadas por orden alfabético");


            for (int i = 0; i < maxCiudades; i++)
            {
                Console.WriteLine(arrayCiudades[i]);
            }

            Console.Read();
        }
    }
}
