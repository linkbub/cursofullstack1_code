using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Actividad3
{
    class Program
    {
        static void Main(string[] args)
        {

            int can, B500, B200, B100, B50, B20, B10, B5;
            B500 = 0;
            B200 = 0;
            B100 = 0;
            B50 = 0;
            B20 = 0;
            B10 = 0;
            B5 = 0;
            can = 0;

            int masComida;
            bool seguir = true;
            bool comidaExiste = false;
            string[] arrayMenu = new string[5];
            int[] arrayPrecio = new int[5];

            Console.OutputEncoding = System.Text.Encoding.UTF8;
            List<string> listComida = new List<string>();

            for (int i = 0; i < arrayMenu.Length; i++)
            {
                Console.Write("Introduce un plato: ");
                arrayMenu[i] = Console.ReadLine();
                Console.Write("Precio del plat: ");
                arrayPrecio[i] = int.Parse(Console.ReadLine());
            }

            for (int i = 0; i < arrayMenu.Length; i++)
            {
                Console.Write("Carta de comida: \n");
                Console.Write(arrayMenu[i] + ": " + arrayPrecio[i] + "€;" + " \n");
            }

            while (seguir)
            {
                Console.Write("Que quieres comer? \n");
                listComida.Add(Console.ReadLine());
                Console.Write("Quieres pedir más comida? (1:Si / 0:No): ");
                masComida = int.Parse(Console.ReadLine());

                if (masComida == 0)
                {
                    seguir = false;
                }
            }

            foreach (var item in listComida)
            {
                for (int i = 0; i < arrayMenu.Length; i++)
                {
                    if (item == arrayMenu[i])
                    {
                        can += arrayPrecio[i];
                        comidaExiste = true;
                    }

                }
                if (!comidaExiste)
                {
                    Console.WriteLine("No tenemos este plato: " + item);
                }
                comidaExiste = false;
            }
            Console.WriteLine("El precio total de tu comida es de: " + can + "€;");


            Console.Read();
        }
    }
}
