using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Actividad4
{
    class Program
    {
        static void Main(string[] args)
        {
            char[] letrasNombre = { 'n', 'o', 'm', 'b', 'r', 'e' };
            char[] letrasApellido = { 'a', 'p', 'e', 'l', 'l', 'i', 'd', 'o' };

            List<char> listaNombre = new List<char>();
            List<char> listaApellido = new List<char>();
            List<char> listaNombreCompleto = new List<char>();
            
            for (int i = 0; i < letrasNombre.Length; i++)
            {
                listaNombre.Add(letrasNombre[i]);
            }

            for (int i = 0; i < letrasApellido.Length; i++)
            {
                listaApellido.Add(letrasApellido[i]);
            }

            listaNombreCompleto.AddRange(listaNombre);
            listaNombreCompleto.Add(' ');
            listaNombreCompleto.AddRange(listaApellido);

            foreach (var letra in listaNombreCompleto)
            {
                Console.Write(letra);
            }
                        
            Console.Read();
        }
    }
}
