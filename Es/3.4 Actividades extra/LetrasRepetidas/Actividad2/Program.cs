using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Actividad2
{
    class Program
    {
        static void Main(string[] args)
        {
            char[] letrasNombre = { 'n', 'o', 'm', 'b', 'r', 'e' };
            List<char> listaNombre = new List<char>();

            for (int i = 0; i < letrasNombre.Length; i++)
            {
                listaNombre.Add(letrasNombre[i]);
            }

            foreach (var nombre in listaNombre)
            {
                switch (nombre)
                {
                    case 'a':
                    case 'e':
                    case 'i':
                    case 'o':
                    case 'u':
                        Console.WriteLine($"La letra {nombre} es vocal");
                        break;

                    case '0':
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                        Console.WriteLine($"La letra {nombre} es un numero y los nombre no contienen numeros!");
                        break;
                    default:
                        Console.WriteLine($"La letra {nombre} es consonante");
                        break;
                }
            }
            Console.Read();
        }
    }
}
