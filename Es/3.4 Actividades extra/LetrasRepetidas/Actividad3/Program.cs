using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Actividad3
{
    class Program
    {
        static void Main(string[] args)
        {
            char[] letrasNombre = {'n', 'o', 'm', 'b', 'r', 'e' };
            List<char> listaNombre = new List<char>();

            for (int i = 0; i < letrasNombre.Length; i++)
            {
                listaNombre.Add(letrasNombre[i]);
            }

            Dictionary<char, int> diccionarioNombre = new Dictionary<char, int>();

            foreach (var nombre in listaNombre)
            {
                if(!diccionarioNombre.ContainsKey(nombre))
                    diccionarioNombre.Add(nombre, 1);
                else
                {
                    diccionarioNombre[nombre]++;
                }
            }

            foreach (var nombre in diccionarioNombre)
            {
                Console.WriteLine(nombre);
            }
            Console.Read();
        }
    }
}
