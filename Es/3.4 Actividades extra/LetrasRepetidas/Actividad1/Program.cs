using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Actividad1
{
    class Program
    {
        static void Main(string[] args)
        {
            char[] letrasNombre = { 'n', 'o', 'm', 'b', 'r', 'e' };

            for (int i = 0; i < letrasNombre.Length; i++)
            {
                Console.Write(letrasNombre[i]);
            }
            Console.Read();
        }
    }
}
