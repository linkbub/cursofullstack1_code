using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Actividad4
{
    class Program
    {
        const int bisiestoCada = 4;

        static void Main(string[] args)
        {
            string nombre = "Leo";
            string apellido1 = "Suarez";
            string apellido2 = "Puyol";
            string nombreCompleto = nombre + " " + apellido1 + " " + apellido2;

            int dia = 25;
            int mes = 06;
            int año = 1989;
            string añoCompleto = dia + "/" + mes + "/" + año;

            string añoCierto = "El año de nacimiento es bisiesto";
            string añoFalso = "El año de nacimiento no es bisiesto";

            int añoPrincipal = 1948;
            bool verificarAño = false;

            for (int i = añoPrincipal; i < año; i += bisiestoCada)
            {
                if (año == añoPrincipal)
                {
                    verificarAño = true;
                }
            };

            Console.Write("El meu nom és " + nombreCompleto + "\n");
            Console.Write("Vaig néixer el " + añoCompleto + "\n");
            if (verificarAño == true)
            {
                Console.Write(añoCierto);
            }
            else
            {
                Console.Write(añoFalso);
            }

            Console.Read();
        }
    }
}
