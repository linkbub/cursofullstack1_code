using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Actividad3
{
    class Program
    {
        const int bisiestoCada = 4;

        static void Main(string[] args)
        {
            int añoPrincipal = 1948;
            int añoNacimiento = 1989;
            string añoCierto = "El año de nacimiento es bisiesto";
            string añoFalso = "El año de nacimiento no es bisiesto";
            bool verificarAño = false;

            for (int i = añoPrincipal; i < añoNacimiento; i += bisiestoCada)
            {
                Console.Write(i + "\n");
                if (añoNacimiento == añoPrincipal)
                {
                    verificarAño = true;
                }
            };

            if (verificarAño == true)
            {
                Console.Write(añoCierto);
            }
            else
            {
                Console.Write(añoFalso);
            }

            Console.Read();
        }
    }
}
