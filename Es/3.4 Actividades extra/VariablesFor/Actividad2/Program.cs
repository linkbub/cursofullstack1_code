using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Actividad2
{
    class Program
    {
        const int añoBisiesto = 1948;
        const int bisiestoCada = 4;

        static void Main(string[] args)
        {
            int añoNacimiento = 1989;
            int cantidadAñosBisiestos = (añoNacimiento - añoBisiesto) / bisiestoCada;

            Console.Write(cantidadAñosBisiestos);
            Console.Read();
        }
    }
}
