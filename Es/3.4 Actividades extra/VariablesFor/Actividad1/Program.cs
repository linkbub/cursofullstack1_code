using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Actividad1
{
    class Program
    {
        static void Main(string[] args)
        {
            string nombre = "Leo";
            string apellido1 = "Suarez";
            string apellido2 = "Puyol";

            int dia = 25;
            int mes = 06;
            int año = 1989;

            Console.Write(apellido1 + " " + apellido2 + ", " + nombre + "\n");
            Console.Write(dia + "/" + mes + "/" + año);

            Console.ReadLine();
        }
    }
}
