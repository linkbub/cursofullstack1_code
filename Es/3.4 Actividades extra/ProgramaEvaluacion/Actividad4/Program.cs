using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Actividad4
{
    class Program
    {
        const string excelente = "Excelente";
        const string notable = "Notable";
        const string suficiente= "Suficiente";
        const string insuficiente = "Insuficiente";

        static void Main(string[] args)
        {
            double puntosObtenidosPorPunto = 0;
            double puntosObtenidosPorActividad = 0;
            double puntosObtenidosTotales = 0;
            int respuesta;
            bool seguirActividad = true;
            bool seguirSubActividad = true;
            string nombreActividad = "";
            string nombreSubActividad = "";
            string nombrePractica = "";


            Console.Write("Introduce el nombre de la practica a evaluar: \n");
            nombrePractica = Console.ReadLine();

            while (seguirActividad)
            {
                Console.Write("Introduce el nombre de la activitat: \n");
                nombreActividad = Console.ReadLine();

                while (seguirSubActividad)
                {
                    Console.Write("Introduce el punto de la activitat: \n");
                    nombreSubActividad = Console.ReadLine();

                    Console.Write("Introduce la puntuación obtenida en este punto: \n");
                    puntosObtenidosPorPunto = double.Parse(Console.ReadLine());

                    Console.Write("Quereis seguir añadiendo puntos a esta actividad? (No:0 / Si:1) \n");
                    respuesta = int.Parse(Console.ReadLine());

                    if (respuesta == 1)
                    {
                        puntosObtenidosPorActividad += puntosObtenidosPorPunto;
                    }
                    else if (respuesta == 0)
                    {
                        puntosObtenidosPorActividad += puntosObtenidosPorPunto;
                        seguirSubActividad = false;
                    }
                }

                Console.Write("Quereis seguir añadiendo actividades? (No:0 / Si:1) \n");
                respuesta = int.Parse(Console.ReadLine());

                if (respuesta == 1)
                {
                    puntosObtenidosTotales += puntosObtenidosPorActividad;
                    puntosObtenidosPorActividad = 0;
                    seguirSubActividad = true;
                }
                else if (respuesta == 0)
                {
                    puntosObtenidosTotales += puntosObtenidosPorActividad;
                    seguirActividad = false;
                }
            }

            Console.Write("Puntuación total obtenida: " + puntosObtenidosTotales + "\n");
            if (puntosObtenidosTotales >= 9)
            {
                Console.Write("Valoración final: " + excelente);
            }
            else if (puntosObtenidosTotales < 9 && puntosObtenidosTotales >= 7)
            {
                Console.Write("Valoración final: " + notable);
            }
            else if (puntosObtenidosTotales < 7 && puntosObtenidosTotales >= 5)
            {
                Console.Write("Valoración final: " + suficiente);
            }
            else if (puntosObtenidosTotales < 5)
            {
                Console.Write("Valoración final: " + insuficiente);
            }

            Console.Read();
        }
    }
}
