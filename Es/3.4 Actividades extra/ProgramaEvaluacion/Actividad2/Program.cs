using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Actividad2
{
    class Program
    {
        static void Main(string[] args)
        {
            double puntosObtenidosPorPunto = 0;
            double puntosObtenidosPorActividad = 0;
            double puntosObtenidosTotales = 0;
            int respuesta;
            bool seguirActividad = true;
            bool seguirSubActividad = true;
            string nombreActividad = "";
            string nombreSubActividad = "";


            Console.Write("Introduce el nombre de la practica a evaluar: \n");
            string nombrePractica = Console.ReadLine();

            while (seguirActividad)
            {
                Console.Write("Introduce el nombre de la actividad: \n");
                nombreActividad = Console.ReadLine();

                while (seguirSubActividad)
                {
                    Console.Write("Introduce el punto de la actividad: \n");
                    nombreSubActividad = Console.ReadLine();

                    Console.Write("Quereis seguir añadiendo puntos a la actividad? (No:0 / Si:1) \n");
                    respuesta = int.Parse(Console.ReadLine());
                }
                Console.Write("Quereis seguir añadiendo actividades? (No:0 / Si:1) \n");
                respuesta = int.Parse(Console.ReadLine());
            }

            Console.Read();
        }
    }
}
