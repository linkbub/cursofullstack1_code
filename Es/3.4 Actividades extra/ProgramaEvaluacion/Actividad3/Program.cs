using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Actividad3
{
    class Program
    {
        static void Main(string[] args)
        {
            double puntosObtenidosPorPunto = 0;
            double puntosObtenidosPorActividad = 0;
            double puntosObtenidosTotales = 0;
            int respuesta;
            bool seguirActividad = true;
            bool seguirSubActividad = true;
            string nombreActividad = "";
            string nombreSubActividad = "";
            string nombrePractica = "";


            Console.Write("Introduce el nombre de la practica a evaluar: \n");
            nombrePractica = Console.ReadLine();

            while (seguirActividad)
            {
                Console.Write("Introduce el nombre de la actividad: \n");
                nombreActividad = Console.ReadLine();

                while (seguirSubActividad)
                {
                    Console.Write("Introduce el punto de la actividad: \n");
                    nombreSubActividad = Console.ReadLine();

                    Console.Write("Introduce la puntuación obtenida en este punto: \n");
                    puntosObtenidosPorPunto = double.Parse(Console.ReadLine());

                    Console.Write("Quereis seguir añadiendo puntos a la actividad? (No:0 / Si:1) \n");
                    respuesta = int.Parse(Console.ReadLine());

                    if (respuesta == 1)
                    {
                        puntosObtenidosPorActividad += puntosObtenidosPorPunto;
                    }
                    else if (respuesta == 0)
                    {
                        puntosObtenidosPorActividad += puntosObtenidosPorPunto;
                        seguirSubActividad = false;
                    }
                }

                Console.Write("Quereis seguir añadiendo actividades? (No:0 / Si:1) \n");
                respuesta = int.Parse(Console.ReadLine());

                if (respuesta == 1)
                {
                    puntosObtenidosTotales += puntosObtenidosPorActividad;
                    puntosObtenidosPorActividad = 0;
                    seguirSubActividad = true;
                }
                else if (respuesta == 0)
                {
                    puntosObtenidosTotales += puntosObtenidosPorActividad;
                    seguirActividad = false;
                }
            }

            Console.Read();
        }
    }
}
