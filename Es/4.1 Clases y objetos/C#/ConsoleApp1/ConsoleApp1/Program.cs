﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            // forma 1
            var pepe = new Persona();
            pepe.Name = "Pepe";
            pepe.Year = 1978;
            pepe.Email = "a@a.com";

            // forma 2
            var lolo = new Persona("lolo", 2019, "b@b");

            // forma 3
            var maria = new Persona
            {
                Name = "María",
                Year = 1987,
                Email = "m@m.com"
            };

            // forma 1 y 3 son iguales

            ShowPersonaInformation(pepe);
            ShowPersonaInformation(lolo);
            ShowPersonaInformation(maria);
            
            Console.WriteLine();

            pepe.ShowInfo();
            lolo.ShowInfo();
            maria.ShowInfo();
        }

        // esto de aquí es una función
        static void ShowPersonaInformation(Persona persona)
        {
            Console.WriteLine("función estática: la persona con nombre " + persona.Name
                + " que nació en el año del señor " + persona.Year
                + " y que tiene de email " + persona.Email);
        }
    }

    class Persona
    {
        public string Name { get; set; }

        public int Year { get; set; }

        public string Email { get; set; }

        public Persona()
        {

        }

        public Persona(string name, int year, string email)
        {
            this.Name = name;
            this.Year = year;
            this.Email = email;
        }


        // esto es un método
        public void ShowInfo()
        {
            Console.WriteLine("método: la persona con nombre " + this.Name
                    + " que nació en el año del señor " + this.Year
                    + " y que tiene de email " + this.Email);
        }

        // esto es un overload (sobrecarga) del método anterior
        public void ShowInfo(bool keepSecrets)
        {

        }
    }
}
