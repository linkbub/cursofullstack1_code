﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Escriba un número");
            var inputText = Console.ReadLine();

            try
            {
                var num1 = int.Parse(inputText);
            }
            catch (System.Exception e1)
            {
                Console.WriteLine($"El formato de número[{inputText}] no es correcto");
            }

            int num2;
            bool conversionWasOk = int.TryParse(inputText, out num2);

            if (conversionWasOk)
                Console.WriteLine($"El número[{num2}] se convirtió corréctamente");
            else
                Console.WriteLine($"El formato de número[{inputText}] no es correcto");
        }       

        static void Declaration1()
        {
            int num1 = 5;
            double num2 = 5.0;

            bool condition1 = true;
            bool condition2 = false;

            string text = "pomelo";
            char c1 = 'p';

            DateTime fecha = new DateTime();
            DateTime fechaDeHoy = DateTime.Now;
        }

        static void Declaration2()
        {
            var num1 = 5;
            var num2 = 5.0;

            var condition1 = true;
            var condition2 = false;

            var text = "pomelo";
            var c1 = 'p';

            var fecha = new DateTime();
            var fechaDeHoy = DateTime.Now;
        }
    }
}
