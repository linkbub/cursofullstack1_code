﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var nombre = "Gandalf";
            SayHello(nombre);
            SayHello();

            var suma = suma(5, 76);
            Console.WriteLine($"la suma es{suma}");

            Console.Read();
        }

        public static void SayHello(string name)
        {
            Console.WriteLine("Hola " + name);
            Console.WriteLine($"Hola {name}");
            Console.WriteLine(string.Format("Hola {0}", name));
        }

        public static int Sum(int num1, int num2)
        {
            var suma = num1 + num2;
            return suma;
        }

        public static void SayHello()
        {
            Console.WriteLine("Hola");
        }

    }
}