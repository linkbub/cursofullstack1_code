using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class Program
    {
        static List<double> Marks { get; set; }
        static string EscapeWord = "RUNYOUFOOLS";

        static void Main(string[] args)
        {
            Console.WriteLine("Bienvenid@ al programa para gestión de alumnos");
            Console.WriteLine("Para introducir notas de los alumnos use la opción a");
            Console.WriteLine("Para obtener estadísticas use la opción e");

            Marks = new List<double>();

            var notasDeAlumnos = new List<double>();

            var keepdoing = true;

            while (keepdoing)
            {
                var option = Console.ReadKey().KeyChar;

                if (option == 'a')
                {

                    ShowStudentsMarksMenu();
                }
                else if (option == 'e')
                {
                    ShowStatsMenu();
                }
            }
            ShowMainMenu();
        }

        static void ShowMainMenu()
        {

            Console.WriteLine("Vuelta al menú principal");
            Console.WriteLine("Para introducir notas de los alumnos use la opción a");
            Console.WriteLine("Para obtener estadísticas use la opción e");
        }

        static void ShowStudentsMarksMenu()
        {
            Console.WriteLine();
            Console.WriteLine("--Menu de Notas--");

            Console.WriteLine("Para ver todas las notasescriba all");
            Console.WriteLine("Para añadir una nueva nota escriba la nota");
            Console.WriteLine("Para volver al menú principal escriba m");

            var keepdoing = true;

            while (keepdoing)
            {
                var text = Console.ReadLine();

                switch (text)
                {
                    case "all":
                        ShowAllStudentsMarks();
                        break;
                    case "m":
                        keepdoing = false;
                        break;
                    default:
                        AddMark(text);
                        break;
                }
            }
            ShowMainMenu();
        }

        static void ShowAllStudentsMarks()
        {
            foreach (var mark in Marks)
            {
                Console.WriteLine(mark);
            }
        }

        static void AddMark(string text)
        {
            var mark = 0.0;

            if (double.TryParse(text, out mark))
            {
                Marks.Add(mark);
                Console.WriteLine("Nota introducida correctamente, añada otra nota más, pulse all para listar todas las notas o pulse m para volver al menú principal");
            }
            else
            {
                Console.WriteLine($"La nota introducida {text} no está en un formato correcto, vuelva a introducirla correctamente");
            }
        }

        static void ShowStatsMenu()
        {
            Console.WriteLine();
            Console.WriteLine("--Menu de Estadísticas--");

            Console.WriteLine("Para ver la media escriba avg");
            Console.WriteLine("Para ver la nota más alta escriba max");
            Console.WriteLine("Para ver la nota más baja escriba min");
            Console.WriteLine("Para volver al menú principal escriba m");

            var keepdoing = true;

            while (keepdoing)
            {
                var text = Console.ReadLine();

                switch (text)
                {
                    case "avg":
                        ShowAverage();
                        break;
                    case "max":
                        ShowMaximum();
                        break;
                    case "min":
                        ShowMinimum();
                        break;
                    case "m":
                        keepdoing = false;
                        break;
                    default:
                        Console.WriteLine("Comando no reconocido, introduzca una opción válida");
                        break;
                }
            }
            ShowMainMenu();
        }

        static void ShowAverage()
        {
            var suma = 0.0;
            for (var i = 0; i < Marks.Count; i++)
            {
                suma += Marks[i];
            }

            var average = suma / Marks.Count;
            Console.WriteLine("La media de los examenes es: {0}", average);
        }

        static void ShowMaximum()
        {
            var max = 0.0;
            for (var i = 0; i < Marks.Count; i++)
            {
                if (Marks[i] > max)
                {
                    max = Marks[i];
                }
            }
            
            Console.WriteLine("La nota más alta es: {0}", max);
        }

        static void ShowMinimum()
        {
            var min = Marks.Count == 0 ? 0.0 : Marks[0];

            for (var i = 0; i < Marks.Count; i++)
            {
                if (Marks[i] < min)
                {
                    min = Marks[i];
                }
            }
            Console.WriteLine("La nota más baja es: {0}", min);
        }
    }
}